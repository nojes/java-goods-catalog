import org.junit.Test;
import org.junit.runner.RunWith;
import org.nojes.App;
import org.nojes.dao.UserDAO;
import org.nojes.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class UserDAOTest {

    @Autowired
    UserDAO userDAO;

    @Test
    public void testSaveAndGetAndDelete() {
        User user = new User("email@example.com", "password", "Firstname Lastname");
        User userInDb = userDAO.save(user);
        assertNotNull(userInDb);
        assertEquals(user.getEmail(), userInDb.getEmail());
    }
}
