package org.nojes.models;


import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;


public class CabinetAccount {

    @Column(name = "ACCOUNT_ID")
    private Long accountId;

    @Id
    @OneToMany(mappedBy = "ACCOUNT_ID")
    @Column(name = "PRODUCT_BOX_ID", nullable = false)
    private Long productBoxId;

    private ArrayList<ProductBox> productBoxes;

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public long getProductBoxId() {
        return productBoxId;
    }

    public void setProductBoxId(long productBoxId) {
        this.productBoxId = productBoxId;
    }

    public ArrayList<ProductBox> getProductBoxes() {
        return productBoxes;
    }

    public void setProductBoxes(ArrayList<ProductBox> productBoxes) {
        this.productBoxes = productBoxes;
    }

    public void addProductBox(ProductBox productBox) {
        this.productBoxes.add(productBox);
    }
}
