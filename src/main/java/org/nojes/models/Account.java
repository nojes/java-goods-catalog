package org.nojes.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ACCOUNT")
public class Account extends User {

    @Column(name = "CABINET_ID")
    private long cabinetId;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "BIRTH_DATE")
    private long birthDate;

    public Account(String email, String password, String fullName, long cabinetId, String phone, long birthDate) {
        super(email, password, fullName);
        this.cabinetId = cabinetId;
        this.phone = phone;
        this.birthDate = birthDate;
        generateNewId();
    }

    public Account(long cabinetId) {
        this.cabinetId = cabinetId;
    }

    public long getCabinetId() {
        return cabinetId;
    }

    public void setCabinetId(long cabinetId) {
        this.cabinetId = cabinetId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }
}
