package org.nojes.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class EntityBase {

    @Id
    @Column(name = "ID", nullable = false)
    Long id;

    void generateNewId() {
        id = Math.abs(UUID.randomUUID().getLeastSignificantBits());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
