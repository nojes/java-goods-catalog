package org.nojes.dao;

import org.nojes.models.ProductBox;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductBoxDAO extends JpaRepository<ProductBox, Long> {}
