package org.nojes.config;

import com.zaxxer.hikari.HikariConfig;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@EnableJpaRepositories("org.nojes.dao")
@EnableTransactionManagement
@EnableAsync
@EntityScan("org.nojes")
@ConfigurationProperties
public class DbConfig extends HikariConfig {

    @Bean
    public DataSource dataSource() {

        /* U prepoda cho-to ne polichilos` */
//        Properties dataSourceProperties = new Properties();
//        dataSourceProperties.put("user", username);
//        dataSourceProperties.put("password", password);
//        dataSourceProperties.put("url", url);
//        dataSourceProperties.put("driverClassName", driverClassName);
//        Properties configProperties = new Properties();
//        configProperties.put("dataSourceClassName", dataSourceClassName);
//        configProperties.put("dataSourceProperties", dataSourceProperties);
//        HikariConfig hikariConfig = new HikariConfig(configProperties);
//        return new HikariDataSource(hikariConfig);

        /* Caution! Hardcoding */
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setUrl("jdbc:h2:mem:db;DB_CLOSE_DELAY=-1");
        dataSource.setUsername("sa");
        dataSource.setPassword("sa");

        return dataSource;
    }
}
